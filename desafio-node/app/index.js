const express = require('express')
const app = express()
const port = 3000
const config = {
    host: 'db',
    user: 'root',
    password: 'root',
    database:'nodedb'
};
const mysql = require('mysql')
const connection = mysql.createConnection(config)
const sql = `CREATE TABLE IF NOT EXISTS people(name varchar(255))`
const sql2 = `INSERT INTO people(name) values('Allan')`
connection.query(sql)
connection.query(sql2)
connection.end()


app.get('/', (req,res) => {
    const connection = mysql.createConnection(config)
    connection.query("SELECT name FROM people", (err, result, fields) => {
        names = result.map(data => `${data.name}`)
        res.send(`<h1>Full Cycle Rocks!</h1><br>${names}`)

    })
    connection.end()
})

app.listen(port, ()=> {
    console.log('Rodando na porta ' + port)
})