# Desafio fullcycle Docker: GoLang

## Requisitos
* GoLang 1.18

## Como rodar localmente

```shell
go run main.go
```

## Construindo a imagem docker

```
docker build -t allanfvc/codeeducation:<VERSION_TAG> . 
```

## Imagem Docker pronta
A imagem Docker está disponível em [DockerHub](https://hub.docker.com/r/allanfvc/codeeducation).